/****************************************************************
 * 'main.js'
 * Main file for program
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 01-16-2019--12:00:46
 *
 **/

 //formatting variables for margin and layout
var 
margin = {
	left: 50,
	right: 50,
	top: 50,
	bottom: 50
},
this_width = ( 600 - margin.left - margin.right ),
this_height = ( 600 - margin.top - margin.bottom ),
padding = 10,
color = "steelblue";


//setting bounds for the bars in graph
var x_ = d3.scaleBand().range( [0,this_width] );
var y_ = d3.scaleLinear().range( [this_height,0] );

//configure svg canvas and chart area
var svg = d3.select("#chart-area").append("svg")
	.attr("width", this_width + margin.left + margin.right )
	.attr("height", this_height + margin.top + margin.bottom)
	.append("g")
		.attr("transform", 
					"translate(" + margin.left + "," + margin.top + ")"
				);

d3.json("data/revenues.json").then(function (data) {
	data.forEach(function (d) {
		d.revenue = +d.revenue
	});

	x_.domain(data.map( function(d) { return d.month; }));
	y_.domain([0, d3.max( data, function (d) { return d.revenue; })]);

	svg.selectAll("bar")
			.data(data)
		.enter().append("rect")
			.style("fill", color)
			.attr("x", function(d) {return x_(d.month);})
			.attr("width", x_.bandwidth() - padding)
			.attr("y", function(d) {return y_(d.revenue);})
			.attr("height", function(d) {return this_height - y_(d.revenue);});
	svg.append("g")
			.attr("transform", "translate(0," + this_height + ")")
			.call(d3.axisBottom(x_));
	svg.append("g")
			.call(d3.axisLeft(y_));

});//end d3.json