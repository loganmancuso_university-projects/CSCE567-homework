

var svg = d3.select("#chart-area")
    .append("svg")
    .attr("width", "400")
    .attr("height", "400");
d3.json("data/buildings.json").then(function(data){
    console.log(data);
    
    data.forEach(function(d) {
        d.height = +d.height;
    });

    var bars = svg.selectAll("rect")
            .data(data)
        .enter().append("rect")
            .attr("y", 0)
            // i for index d value
            .attr("x", function(d, i){
                return (i * 60);
            })
            .attr("width", 40)
            .attr("height", function(d){
                return d.height;
            })
            .attr("fill", function(d) {
                return "black";
            });

})